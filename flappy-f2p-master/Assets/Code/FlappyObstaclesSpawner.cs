﻿using UnityEngine;
using System.Collections.Generic;

public class FlappyObstaclesSpawner : MonoBehaviour {

	public GameObject obstaclePrefab;

    List<GameObject> spawnedObstacles = new List<GameObject>();

    public int spawnX;
    public int initialSpawnY;
    public float gapHeight;
    public int obstaclesMax;

    public int destroyDistance;

    public Rigidbody body;

    private GameObject lastObstacle;
    private int spawnY;

    private void Update()
    {
        if(spawnedObstacles.Count < obstaclesMax)
        {
            spawnX += Random.Range(7, 10);
            spawnY = initialSpawnY + Random.Range(-3, 4);
            Spawn(spawnX, spawnY, gapHeight);
        }

       if(spawnedObstacles.Count > 0)
        {
            lastObstacle = spawnedObstacles[0];

            if (body.transform.position.x - lastObstacle.transform.position.x > destroyDistance)
            {
                spawnedObstacles.Remove(lastObstacle);
                Destroy(lastObstacle);
            }
        }
    }

    void Spawn( float x, float y, float gapHeight ) {
		GameObject spawned = GameObject.Instantiate( obstaclePrefab );
        spawned.transform.parent = transform;
		spawned.transform.position = new Vector3( x, y, 0 );
		spawnedObstacles.Add( spawned );

        Transform bottomTransform = spawned.transform.Find( "Bottom" );
        Transform topTransform = spawned.transform.Find( "Top" );
        float bottomY = -gapHeight/2;
        float topY = +gapHeight/2;
        bottomTransform.localPosition = Vector3.up * bottomY;
        topTransform.localPosition = Vector3.up * topY;
    }

}
