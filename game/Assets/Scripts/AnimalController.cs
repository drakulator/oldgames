﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalController : MonoBehaviour {

    public float moveSpeed;
    public float timeBetweenMoveMin;
    public float timeBetweenMoveMax;
    public float timeToMove;

    public Transform wallCheck;
    public Transform edgeCheck;
    public float wallCheckRadius;
    public LayerMask whatIsWall;

    private bool hittingWall;
    private bool atEdge;

    private Rigidbody2D rigidBody;
    private Animator anim;
    private int moveDirection; // -1 == left, 1 == right
    private bool isMoving;
    private float timeBetweenMoveCounter;
    private float timeToMoveCounter;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody2D>();

        timeBetweenMoveCounter = Random.Range(timeBetweenMoveMin, timeBetweenMoveMax);
        timeToMoveCounter = Random.Range(timeToMove * 0.75F, timeToMove * 1.25F);
    }

    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            timeToMoveCounter -= Time.deltaTime;
            rigidBody.velocity = new Vector3(moveDirection * moveSpeed, rigidBody.velocity.y, 0f);
            transform.localScale = new Vector3(-moveDirection, 1f, 1f);

            hittingWall = Physics2D.OverlapCircle(wallCheck.position, wallCheckRadius, whatIsWall);
            atEdge = !Physics2D.OverlapCircle(edgeCheck.position, wallCheckRadius, whatIsWall);

            if (timeToMoveCounter < 0f || hittingWall || atEdge)
            {
                isMoving = false;
                anim.SetBool("IsMoving", false);
                timeBetweenMoveCounter = Random.Range(timeBetweenMoveMin, timeBetweenMoveMax);
            }
        }
        else
        {
            timeBetweenMoveCounter -= Time.deltaTime;
            rigidBody.velocity = Vector2.zero;

            if (timeBetweenMoveCounter < 0f)
            {
                isMoving = true;
                anim.SetBool("IsMoving", true);
                timeToMoveCounter = timeToMove;

                if (hittingWall || atEdge)
                    moveDirection = -moveDirection;
                else
                    moveDirection = Random.Range(0, 2) * 2 - 1;
            }
        }
    }

    private void OnDestroy()
    {

    }
}
