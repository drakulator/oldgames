﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveToTarget : MonoBehaviour {

    public GameObject nextWagon;
    public GameObject enemyStation;
    public float deceleration = 1f;

    private Transform targetTransform;
    private NavMeshAgent agent;
    private float speedModifier;
    private bool isLocomotive = false;

	// Use this for initialization
	void Start () {
        agent = GetComponent<NavMeshAgent>();
        if(nextWagon != null)
            targetTransform = nextWagon.transform;
        else
        {
            targetTransform = enemyStation.transform;
            isLocomotive = true;
        }
        speedModifier = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if (nextWagon == null && !isLocomotive)
        {
            speedModifier = deceleration;
            targetTransform = enemyStation.transform;
            isLocomotive = true; //not exactly, but is first
        }
        else
        {
            agent.speed -= speedModifier * Time.deltaTime;
            agent.SetDestination(targetTransform.position);
        }
    }
}
