﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionController : MonoBehaviour {

    public GameObject deathEffect;
    public int damage;

    private Rigidbody thisTrain;
    private Rigidbody otherTrain;

    // Use this for initialization
    void Start () {
        thisTrain = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Blue Train" || other.tag == "Red Train")
        {
            otherTrain = other.GetComponent<Rigidbody>();
            if (otherTrain.mass * otherTrain.velocity.sqrMagnitude >= thisTrain.mass * thisTrain.velocity.sqrMagnitude)
                Destroy(gameObject);
        }

        if ((other.tag == "Red Station" && tag == "Blue Train") || (other.tag == "Blue Station" && tag == "Red Train"))
        {
            other.GetComponent<HealthManager>().GiveDamage(damage);
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        Instantiate(deathEffect, transform.position, transform.rotation);
    }
}
