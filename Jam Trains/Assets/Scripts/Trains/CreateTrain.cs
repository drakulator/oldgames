﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateTrain : MonoBehaviour {

    public GameObject trainPrefab;
    public GameObject enemyStation;
    public int wagonParts;
    public float timeBetween;

    private GameObject nextWagon;
    private GameObject lastWagon;
    private GameObject trainPart;

	// Use this for initialization
	void Start () {
        trainPart = Instantiate(trainPrefab, transform.position, transform.rotation);
        trainPart.GetComponent<MoveToTarget>().enemyStation = enemyStation;
        if (enemyStation.tag == "Red Station")
            trainPart.tag = "Blue Train";
        else
            trainPart.tag = "Red Train";
        StartCoroutine(CreateParts(timeBetween));
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public IEnumerator CreateParts(float timeBetween)
    {
        for (int i = 0; i < wagonParts; i++)
        { 
            yield return new WaitForSeconds(timeBetween);
            lastWagon = trainPart;
            trainPart = Instantiate(trainPrefab, transform.position, transform.rotation);
            trainPart.GetComponent<MoveToTarget>().enemyStation = enemyStation;
            trainPart.GetComponent<MoveToTarget>().nextWagon = lastWagon;
            if (enemyStation.tag == "Red Station")
                trainPart.tag = "Blue Train";
            else
                trainPart.tag = "Red Train";
        }
    }
}

