﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourcesManager : MonoBehaviour {

    public int startResources;
    public int income;

    [SerializeField]
    private int resources;

	// Use this for initialization
	void Start () {
        resources = startResources;
    }
	
	// Update is called once per frame
	void Update () {
        resources += (int)(income * Time.deltaTime);
	}

    void AddResources(int amount)
    {
        resources += amount;
    }
}
