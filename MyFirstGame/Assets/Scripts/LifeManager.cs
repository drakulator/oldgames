﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LifeManager : MonoBehaviour {

    //public int startingLives;

    private int lifeCounter;

    private Text text;

    public GameObject gameOverScreen;

    public PlayerController player;

    public string mainMenu;

    public float waitAfterGameOver;

    void Start()
    {
        player = FindObjectOfType<PlayerController>();

        text = GetComponent<Text>();

        lifeCounter = PlayerPrefs.GetInt("PlayerCurrentLives", 0) ;
	}
	
	// Update is called once per frame
	void Update () {
        if(lifeCounter < 0)
        {
            gameOverScreen.SetActive(true);
            Time.timeScale = 0f;
            player.enabled = false;
        }

        text.text = "x " + lifeCounter;

        if (gameOverScreen.activeSelf)
        {
            waitAfterGameOver -= Time.deltaTime;
        }

        if(waitAfterGameOver < 0)
        {
            SceneManager.LoadScene(mainMenu);
        }
	}

    public void GiveLife()
    {
        lifeCounter++;
        PlayerPrefs.SetInt("PlayerCurrentLives", lifeCounter);
    }

    public void TakeLife()
    {
        lifeCounter--;
        PlayerPrefs.SetInt("PlayerCurrentLives", lifeCounter);
    }
}


