﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public string mainMenu;

    public string levelSelect;

    public bool isPaused;

    public GameObject pauseMenuCanvas;

    public PlayerController player;

    void Start()
    {
        player = FindObjectOfType<PlayerController>();
    }

        void Update()
    {
        if (isPaused)
        {
            pauseMenuCanvas.SetActive(true);
            Time.timeScale = 0f;
            player.enabled = false;
        }
        else
        {
            pauseMenuCanvas.SetActive(false);
            Time.timeScale = 1f;
            player.enabled = true;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isPaused = !isPaused;
        }
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(mainMenu);
    }

    public void LevelSelect()
    {
        SceneManager.LoadScene(levelSelect);
    }

    public void Resume()
    {
        isPaused = false;
    }
}
