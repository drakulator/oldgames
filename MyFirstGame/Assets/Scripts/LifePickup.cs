﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifePickup : MonoBehaviour {

    private LifeManager lifeSystem;

    public AudioSource coinSoundEffect;

    void Start()
    {
        lifeSystem = FindObjectOfType<LifeManager>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerController>() == null)
            return;

        lifeSystem.GiveLife();

        coinSoundEffect.Play();

        Destroy(gameObject);
    }
}