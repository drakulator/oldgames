﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyerEnemyMove : MonoBehaviour {

    private PlayerController player;

    public float moveSpeed;

    public float playerRange;

    public LayerMask playerLayer;

    private bool playerInRange;

	// Use this for initialization
	void Start () {

        player = FindObjectOfType<PlayerController>();

	}
	
	// Update is called once per frame
	void Update () {

        if(!playerInRange)
            playerInRange = Physics2D.OverlapCircle(transform.position, playerRange, playerLayer);

        if (playerInRange)
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, moveSpeed * Time.deltaTime);
	}

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawSphere(transform.position, playerRange);
    }
}
