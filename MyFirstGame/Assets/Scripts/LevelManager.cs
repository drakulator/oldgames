﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

    public GameObject currentCheckpoint;

    private PlayerController player;

    public GameObject deathParticle;
    public GameObject respawnParticle;

    public float respawnDelay;

    public int pointsPenaltyOnDeath;

    public HealthManager healthManager;

    //private float gravityScale;

    //private CameraController camera;

    // Use this for initialization
    void Start () {
        player = FindObjectOfType<PlayerController>();

        healthManager = FindObjectOfType<HealthManager>();

        //camera = FindObjectOfType<CameraController>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void RespawnPlayer()
    {
        StartCoroutine("RespawnPlayerCo");
    }

    public IEnumerator RespawnPlayerCo()
    {
        //gravityScale = player.GetComponent<Rigidbody2D>().gravityScale;

        Instantiate(deathParticle, player.transform.position, player.transform.rotation);
        player.enabled = false;
        player.GetComponent<Renderer>().enabled = false;
        player.gameObject.SetActive(false);
        //camera.isFollowing = false;

        //player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        //player.GetComponent<Rigidbody2D>().gravityScale = 0f;

        ScoreManager.AddPoints(-pointsPenaltyOnDeath);
        Debug.Log("Player Respawn");

        yield return new WaitForSeconds(respawnDelay);

        player.enabled = true;
        player.GetComponent<Renderer>().enabled = true;
        player.gameObject.SetActive(true);
        player.transform.parent = null;
        healthManager.FullHealth();
        healthManager.isDead = false;
        //camera.isFollowing = true;

        //player.GetComponent<Rigidbody2D>().gravityScale = gravityScale;

        player.transform.position = currentCheckpoint.transform.position;
        Instantiate(respawnParticle, currentCheckpoint.transform.position, currentCheckpoint.transform.rotation);
    }
}