﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    public static int score;

    Text text;

    private void Start()
    {
        text = GetComponent<Text>();

        score = PlayerPrefs.GetInt("PlayerCurrentScore", 0);

        text.text = "" + score;
    }

    private void Update()
    {
        if (score < 0)
            score = 0;

        text.text = "" + score;
    }

    public static void AddPoints (int pointsToAdd)
    {
        score += pointsToAdd;

        PlayerPrefs.SetInt("PlayerCurrentScore", score);
    }

    public static void Reset()
    {
        score = 0;

        PlayerPrefs.SetInt("PlayerCurrentScore", 0);
    }
}
