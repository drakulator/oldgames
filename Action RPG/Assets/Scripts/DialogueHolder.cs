﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueHolder : MonoBehaviour {

    public string dialogue;
    public string[] dialogueLines;

    private DialogueManager dMan;

	// Use this for initialization
	void Start () {
        dMan = FindObjectOfType<DialogueManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.name == "Player")
        {
            if (Input.GetKeyUp(KeyCode.Space))
            {
                dMan.ShowBox(dialogue);

                //if (!dMan.dialogueActive)
                //{
                  //  dMan.dialogueLines = dialogueLines;
                    //dMan.currentLine = 0;
                    //dMan.ShowDialogue();w
                //}
            }
        }
    }
}
