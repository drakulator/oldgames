﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthManager : MonoBehaviour {

    public int maxHealth;
    public int health;

	// Use this for initialization
	void Start () {
        if (health == 0)
        {
            health = maxHealth;
        }
	}
	
	// Update is called once per frame
	void Update () {
		if(health <= 0)
        {
            Destroy(gameObject);
        }
	}

    public void GiveDamage(int damage)
    {
        health -= damage;
    }
}
