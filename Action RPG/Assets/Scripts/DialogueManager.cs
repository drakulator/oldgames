﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {

    public GameObject dBox;
    public Text dText;
    public bool dialogueActive;
    public string[] dialogueLines;
    public int currentLine;
    public float waitTime;

    private float waitTimeCounter;


    // Use this for initialization
    void Start () {
        waitTimeCounter = waitTime;
    }
	
	// Update is called once per frame
	void Update () {
        if (dialogueActive && Input.GetKeyDown(KeyCode.Space)) {
            //dBox.SetActive(false);
            //dialogueActive = false;

            currentLine++;
        }

        if (currentLine >= dialogueLines.Length)
        {
            dBox.SetActive(false);
            dialogueActive = false;
            currentLine = 0;
        }
        
        dText.text = dialogueLines[currentLine];
    }

    public void ShowBox(string dialogue) {
        dBox.SetActive(true);
        dialogueActive = true;

        dText.text = "";
        int i = 0;
        while (i < dialogue.Length)

            waitTimeCounter -= Time.deltaTime;

            if(waitTimeCounter < 0)
            {
                waitTimeCounter = waitTime;
                dText.text += dialogue[i];
            }
    }

    public void ShowDialogue()
    {
        dBox.SetActive(true);
        dialogueActive = true;
    }
}
