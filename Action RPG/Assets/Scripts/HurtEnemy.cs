﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtEnemy : MonoBehaviour {

    public int damage;
    public GameObject damageNumber;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            collision.GetComponent<HealthManager>().GiveDamage(damage);
            var clone = (GameObject)Instantiate(damageNumber, transform.position, Quaternion.Euler (Vector3.zero));
            clone.GetComponent<FloatingNumbers>().damage = damage;
        }
    }
}
