﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SlimeController : MonoBehaviour {

    public float moveSpeed;
    public float timeBetweenMove;
    public float timeToMove;
    public float waitToReload;

    private Rigidbody2D rigidBody;
    private Vector3 moveDirection;
    private GameObject player;
    private bool isMoving;
    private bool isReloading;
    private float timeBetweenMoveCounter;
    private float timeToMoveCounter;

	// Use this for initialization
	void Start () {
        rigidBody = GetComponent<Rigidbody2D>();

        timeBetweenMoveCounter = Random.Range(timeBetweenMove * 0.75F, timeBetweenMove * 1.25F);
        timeToMoveCounter = Random.Range(timeToMove * 0.75F, timeToMove * 1.25F);
    }
	
	// Update is called once per frame
	void Update () {
        if (isMoving)
        {
            timeToMoveCounter -= Time.deltaTime;
            rigidBody.velocity = moveDirection;

            if(timeToMoveCounter <  0f)
            {
                isMoving = false;
                timeBetweenMoveCounter = Random.Range(timeBetweenMove * 0.75F, timeBetweenMove * 1.25F);
            }
        }
        else
        {
            timeBetweenMoveCounter -= Time.deltaTime;
            rigidBody.velocity = Vector2.zero;

            if(timeBetweenMoveCounter < 0f)
            {
                isMoving = true;
                timeToMoveCounter = Random.Range(timeToMove * 0.75F, timeToMove * 1.25F);

                moveDirection = new Vector3(Random.Range(-1f, 1f) * moveSpeed, Random.Range(-1f, 1f) * moveSpeed, 0f);
            }
        }

        if (isReloading)
        {
            waitToReload -= Time.deltaTime;
            if(waitToReload < 0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                player.SetActive(true);
            }
        }
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.name == "Player")
        {
            //Destroy(collision.gameObject);

            collision.gameObject.SetActive(false);
            isReloading = true;
            player = collision.gameObject;
        }
    }

    private void OnDestroy()
    {
        
    }
}
