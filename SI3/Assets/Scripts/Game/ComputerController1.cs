﻿using UnityEngine;

public class ComputerController1 : MonoBehaviour
{
    public Algorithm algorithm;

    private GridController gridController;

    // Use this for initialization
    void Start()
    {
        gridController = FindObjectOfType<GridController>();
    }

    public Field FindBestMove()
    {
        return algorithm.FindBestMove(gridController);
    }
}