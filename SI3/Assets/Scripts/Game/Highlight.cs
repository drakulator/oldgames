﻿using System.Collections.Generic;
using UnityEngine;

public class Highlight : MonoBehaviour
{
    public float rHighlight = 0.5f;
    public float bHighlight = 0.5f;

    private List<Color> startColors;
    private bool isHighlighting = false;
    private Field field;
    private GameController gameController;

    // Use this for initialization
    void Start()
    {
        field = GetComponentInParent<Field>();
        gameController = FindObjectOfType<GameController>();

        startColors = new List<Color>();
        foreach (var material in GetComponent<Renderer>().materials)
        {
            startColors.Add(material.color);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isHighlighting && Input.GetMouseButtonDown(0))
        {
            if (!field.PlaceToken())
            {
                Debug.Log("Field already taken!");
            }
            else
            {
                StopHighlighting();
            }
        } 
    }

    private void OnMouseEnter()
    {
        if((gameController.currentPlayer == 1 && gameController.player1.ToString() == "Player") ||
            (gameController.currentPlayer == 2 && gameController.player2.ToString() == "Player"))
                StartHighlighting();
    }

    private void OnMouseExit()
    {
        StopHighlighting();
    }

    public void StartHighlighting()
    {
        startColors = new List<Color>();
        if(gameController.currentPlayer == 1)
        {
            foreach (var material in GetComponent<Renderer>().materials)
            {
                startColors.Add(material.color);
                material.color = new Color(material.color.r + rHighlight, material.color.g, material.color.b);
            }
        }
        else if (gameController.currentPlayer == 2)
        {
            foreach (var material in GetComponent<Renderer>().materials)
            {
                startColors.Add(material.color);
                material.color = new Color(material.color.r, material.color.g, material.color.b + bHighlight);
            }
        }
        isHighlighting = true;
    }

    public void StopHighlighting()
    {
        int i = 0;
        foreach (var material in GetComponent<Renderer>().materials)
        {
            material.color = startColors[i];
            i++;
        }
        isHighlighting = false;
    }
}
