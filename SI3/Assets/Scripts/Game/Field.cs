﻿using UnityEngine;

public class Field : MonoBehaviour
{
    public GameObject redToken;
    public GameObject blueToken;
    public GameObject pointsNumber;
    public bool isTaken = false;

    public int positionX;
    public int positionY;

    [HideInInspector]
    public int grade;

    private GridController gridController;
    private GameController gameController;

    // Use this for initialization
    void Start () {
        gridController = FindObjectOfType<GridController>();
        gameController = FindObjectOfType<GameController>();
    }

    public bool PlaceToken()
    {
        if (isTaken)
            return false;

        isTaken = true;
        if(gameController.currentPlayer == 1)
        {
            redToken.SetActive(true);
            redToken.transform.Rotate(new Vector3(0f, Random.value * 360f, 0f));
        }
        else if (gameController.currentPlayer == 2)
        {
            blueToken.SetActive(true);
            blueToken.transform.Rotate(new Vector3(0f, Random.value * 360f, 0f));
        }
        GivePoints(CountPoints(), gameController.currentPlayer);
        if (gridController.IsGameEnding())
            gameController.EndGame();
        else
            gameController.NextTurn();
        return true;
    }

    public int CountPoints()
    {
        int points = 0;
        //-----------------------------------------------------------------
        bool isVerticalLineClosed = true;
        for (int i = 0; i < gridController.boardSize; i++)
        {
            if (!gridController.grid[i, positionY].isTaken)
            {
                isVerticalLineClosed = false;
                break;
            }
        }
        //-----------------------------------------------------------------
        bool isHorizontalLineClosed = true;
        for (int j = 0; j < gridController.boardSize; j++)
        {
            if (!gridController.grid[positionX, j].isTaken)
            {
                isHorizontalLineClosed = false;
                break;
            }
        }
        //-----------------------------------------------------------------
        bool isDiagonalLine1Closed = true;
        int diagonalLine1Points = 1;
        for (int i = positionX + 1, j = positionY + 1; i < gridController.boardSize && j < gridController.boardSize; i++, j++)
        {
            if (!gridController.grid[i, j].isTaken)
            {
                isDiagonalLine1Closed = false;
                break;
            }
            diagonalLine1Points++;
        }
        if (isDiagonalLine1Closed)
        {
            for (int i = positionX - 1, j = positionY - 1; i >= 0 && j >= 0; i--, j--)
            {
                if (!gridController.grid[i, j].isTaken)
                {
                    isDiagonalLine1Closed = false;
                    break;
                }
                diagonalLine1Points++;
            }
        }
        //-----------------------------------------------------------------
        bool isDiagonalLine2Closed = true;
        int diagonalLine2Points = 1;
        for (int i = positionX - 1, j = positionY + 1; i >= 0 && j < gridController.boardSize; i--, j++)
        {
            if (!gridController.grid[i, j].isTaken)
            {
                isDiagonalLine2Closed = false;
                break;
            }
            diagonalLine2Points++;
        }
        if (isDiagonalLine2Closed)
        {
            for (int i = positionX + 1, j = positionY - 1; i < gridController.boardSize && j >= 0; i++, j--)
            {
                if (!gridController.grid[i, j].isTaken)
                {
                    isDiagonalLine2Closed = false;
                    break;
                }
                diagonalLine2Points++;
            }
        }
        //-----------------------------------------------------------------
        if (isVerticalLineClosed)
            points += gridController.boardSize;
        if (isHorizontalLineClosed)
            points += gridController.boardSize;
        if (isDiagonalLine1Closed && diagonalLine1Points > 1)
            points += diagonalLine1Points;
        if (isDiagonalLine2Closed && diagonalLine2Points > 1)
            points += diagonalLine2Points;
        return points;
    }

    public void GivePoints(int points, int player)
    {
        if(points > 0)
        {
            if(player == 1)
            {
                gameController.redPlayerPoints += points;
                Debug.Log(points + " for Red Player!");
            }
            else if(player == 2)
            {
                gameController.bluePlayerPoints += points;
                Debug.Log(points + " for Blue Player!");
            }
            var clone = Instantiate(pointsNumber);
            clone.GetComponent<FloatingNumbers>().displayNumber.text = "" + points;
            clone.transform.position = new Vector3(transform.position.x - 4f, transform.position.y + 1f, transform.position.z + 1f);
            if (player == 1)
                clone.GetComponent<FloatingNumbers>().displayNumber.color = Color.red;
            else if (player == 2)
                clone.GetComponent<FloatingNumbers>().displayNumber.color = Color.blue;
            clone.SetActive(true);
        }
    }
}
