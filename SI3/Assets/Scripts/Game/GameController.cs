﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum Controller
{
    Player,
    Computer
}

public enum SearchNode
{
    StartLeftDown,
    Random,
    RandomFirst
}

public enum CheckGameStatus
{
    Points,
    PointsDepth,
    PointsRandom,
    PointsDepthRandom,
    QuickPoints
}

public class GameController : MonoBehaviour
{
    public int redPlayerPoints;
    public int bluePlayerPoints;

    public Controller player1;
    public Controller player2;

    public int currentPlayer;
    public float timeBetweenComputerMoves;

    public Text redPlayerScore;
    public Text bluePlayerScore;
    public GameObject endGameScreen;

    private ComputerController1 controller1;
    private ComputerController2 controller2;
    private bool hasGameEnded = false;

    private float startTime = 0f;

    private float totalTime1 = 0f;
    private float totalTime2 = 0f;

    private int moveCount1 = 0;
    private int moveCount2 = 0;

    // Use this for initialization
    void Start () {
        controller1 = FindObjectOfType<ComputerController1>();
        controller2 = FindObjectOfType<ComputerController2>();
        redPlayerPoints = 0;
        bluePlayerPoints = 0;

        if ((currentPlayer == 1 && player1.ToString() == "Computer" && !hasGameEnded) ||
            (currentPlayer == 2 && player2.ToString() == "Computer" && !hasGameEnded))
        {
            StartCoroutine(ComputerMove());
        }
    }

    // Update is called once per frame
    void Update()
    {
        redPlayerScore.text = "" + redPlayerPoints;
        bluePlayerScore.text = "" + bluePlayerPoints;
    }

    public void SwapPlayers()
    {
        if(currentPlayer == 1)
        {
            totalTime1 += (Time.realtimeSinceStartup - startTime);
            moveCount1++;
        }
        else
        {
            totalTime2 += (Time.realtimeSinceStartup - startTime);
            moveCount2++;
        }
        currentPlayer = (currentPlayer == 1 ? 2 : 1);
        Debug.Log("Player " + currentPlayer + " turn");
    }

    public void NextTurn()
    {
        SwapPlayers();
        startTime = Time.realtimeSinceStartup;
        if (((currentPlayer == 1 && player1.ToString() == "Computer") || 
            (currentPlayer == 2 && player2.ToString() == "Computer")) && !hasGameEnded)
        {
            StartCoroutine(ComputerMove());
        }
    }

    public void EndGame()
    {
        if(redPlayerPoints > bluePlayerPoints)
        {
            endGameScreen.GetComponentInChildren<Text>().text = "Game ended! Red Player wins!";
            endGameScreen.GetComponentInChildren<Text>().color = Color.red;
        }
        else if (redPlayerPoints < bluePlayerPoints)
        {
            endGameScreen.GetComponentInChildren<Text>().text = "Game ended! Blue Player wins!";
            endGameScreen.GetComponentInChildren<Text>().color = Color.blue;
        }
        else
            endGameScreen.GetComponentInChildren<Text>().text = "Game ended in a draw!";
        Debug.Log("Red Player average move time: " + totalTime1 / moveCount1);
        Debug.Log("Blue Player average move time: " + totalTime2 / moveCount2);
        endGameScreen.SetActive(true);
        hasGameEnded = true;
    }

    private IEnumerator ComputerMove()
    {
        yield return new WaitForSeconds(timeBetweenComputerMoves);

        if(currentPlayer == 1)
            controller1.FindBestMove().PlaceToken();
        else if (currentPlayer == 2)
            controller2.FindBestMove().PlaceToken();
    }
}
