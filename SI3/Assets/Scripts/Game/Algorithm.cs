﻿using UnityEngine;

public abstract class Algorithm : ScriptableObject
{
    public int maxDepth;
    public abstract Field FindBestMove(GridController gridController);
    public int maxIterations;

    [Header("Minmax iteration might take longer")]
    public float maxMoveTime;
    public CheckGameStatus checkGameStatus;
    public SearchNode searchNode;

    protected Field[,] board;
    protected float timer;
    protected float startTime;
    protected int iterations;

}
