﻿using UnityEngine;

public class GridController : MonoBehaviour
{
    public GameObject fieldPrefab;
    public int boardSize = 10;
    [HideInInspector]
    public Field[,] grid;

    private float tileSize;

    // Use this for initialization
    private void Start()
    {
        grid = new Field[boardSize, boardSize];
        tileSize = 10f / boardSize;
        DrawBoard();
    }

    private void DrawBoard()
    {
        for (int i = 0; i < boardSize; i++)
        {
            Vector3 startForward = Vector3.forward * i * tileSize;
            for (int j = 0; j < boardSize; j++)
            {
                Vector3 startRight = Vector3.right * j * tileSize;
                GameObject field = Instantiate(fieldPrefab);
                field.name = "Field" + i + j;
                field.transform.localScale = new Vector3(1f / boardSize, 0.1f, 1f / boardSize);
                field.transform.position = new Vector3(startRight.x, 0f, startForward.z);
                field.GetComponent<Field>().positionX = i;
                field.GetComponent<Field>().positionY = j;
                grid[i, j] = field.GetComponent<Field>();
            }
        }
    }

    public bool IsGameEnding()
    {
        foreach (Field field in grid)
        {
            if (!field.isTaken)
                return false;
        }
        return true;
    }
}