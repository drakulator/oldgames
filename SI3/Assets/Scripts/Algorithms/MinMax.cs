﻿using UnityEngine;
using System;

[CreateAssetMenu (fileName = "MinMax", menuName = "Algorithms/MinMax")]
public class MinMax : Algorithm
{
    public int Minmax(Field[,] board, int depth, bool isLookingforMax, GridController gridController, int pointsSum = 0)
    {
        if ((gridController.IsGameEnding() || depth > maxDepth || iterations > maxIterations) && isLookingforMax)
        {
            if (checkGameStatus.ToString() == "PointsDepth")
                return (pointsSum - depth);
            else if (checkGameStatus.ToString() == "PointsRandom")
                return (pointsSum + (int)((0.5 - UnityEngine.Random.value) * pointsSum * 0.5));
            else if (checkGameStatus.ToString() == "PointsDepthRandom")
                return (pointsSum + (int)((0.5 - UnityEngine.Random.value) * pointsSum * 0.5) - depth);
            else
                return pointsSum;
        }
        if ((gridController.IsGameEnding() || depth > maxDepth || iterations > maxIterations) && !isLookingforMax)
        {
            if (checkGameStatus.ToString() == "PointsDepth")
                return (pointsSum + depth);
            else if (checkGameStatus.ToString() == "PointsRandom")
                return (pointsSum + (int)((0.5 - UnityEngine.Random.value) * pointsSum * 0.5));
            else if (checkGameStatus.ToString() == "PointsDepthRandom")
                return (pointsSum + (int)((0.5 - UnityEngine.Random.value) * pointsSum * 0.5) + depth);
            else
                return pointsSum;
        }
        int value;
        int bestValue;
        if (isLookingforMax)
        {
            bestValue = Int32.MinValue;
            foreach (Field field in board)
            {
                if (!field.isTaken)
                {
                    field.isTaken = true;

                    if (checkGameStatus.ToString() == "QuickPoints")
                        pointsSum += field.CountPoints() / depth;
                    else
                        pointsSum += field.CountPoints();
                    iterations++;
                    value = Minmax(board, depth + 1, false, gridController, pointsSum);
                    bestValue = Mathf.Max(value, bestValue);

                    field.isTaken = false;
                }
            }
        }
        else
        {
            bestValue = Int32.MaxValue;
            foreach (Field field in board)
            {
                if (!field.isTaken)
                {
                    field.isTaken = true;

                    pointsSum -= field.CountPoints();
                    iterations++;
                    value = Minmax(board, depth + 1, true, gridController, pointsSum);
                    bestValue = Mathf.Min(value, bestValue);

                    field.isTaken = false;
                }
            }
        }
        return bestValue;
    }

    public override Field FindBestMove(GridController gridController)
    {
        board = gridController.grid;
        iterations = 0;
        Field bestMove = null;
        int bestGrade = Int32.MinValue;
        int moveGrade = 0;
        startTime = Time.realtimeSinceStartup;
        timer = maxMoveTime;

        if(searchNode.ToString() == "RandomFirst")
        {
            SearchRandomNode(board, ref moveGrade, ref bestGrade, ref bestMove, gridController);
        }

        if (searchNode.ToString() == "Random")
        {
            SearchRandomNodes(board, ref moveGrade, ref bestGrade, ref bestMove, gridController);
        }

        foreach (Field field in board)
        {
            if (timer <= 0 && bestMove != null)
                break;
            if (!field.isTaken)
            {
                field.isTaken = true;

                moveGrade = Minmax(board, 0, false, gridController);

                field.isTaken = false;

                if (moveGrade > bestGrade)
                {
                    bestGrade = moveGrade;
                    bestMove = field;
                }
            }
            timer -= (Time.realtimeSinceStartup - startTime);
        }
        return bestMove;
    }

    private void SearchRandomNode(Field[,] board, ref int moveGrade, ref int bestGrade, ref Field bestMove, GridController gridController)
    {
        int x = UnityEngine.Random.Range(0, gridController.boardSize - 1);
        int y = UnityEngine.Random.Range(0, gridController.boardSize - 1);

        if (!board[x, y].isTaken)
        {
            board[x, y].isTaken = true;

            moveGrade = Minmax(board, 0, false, gridController);

            board[x, y].isTaken = false;

            if (moveGrade > bestGrade)
            {
                bestGrade = moveGrade;
                bestMove = board[x, y];
            }
        }
        timer -= (Time.realtimeSinceStartup - startTime);
    }

    private void SearchRandomNodes(Field[,] board, ref int moveGrade, ref int bestGrade, ref Field bestMove, GridController gridController)
    {
        bool[,] boardChecked = new bool[gridController.boardSize, gridController.boardSize];
        boardChecked.Initialize(); //all set to false

        while (timer > 0)
        {
            int x = UnityEngine.Random.Range(0, gridController.boardSize - 1);
            int y = UnityEngine.Random.Range(0, gridController.boardSize - 1);
            if (!boardChecked[x, y])
            {
                boardChecked[x, y] = true;
                if (!board[x, y].isTaken)
                {

                    board[x, y].isTaken = true;

                    moveGrade = Minmax(board, 0, false, gridController);

                    board[x, y].isTaken = false;

                    if (moveGrade > bestGrade)
                    {
                        bestGrade = moveGrade;
                        bestMove = board[x, y];
                    }
                }
            }
            timer -= (Time.realtimeSinceStartup - startTime);
        }
    }
}
